# DevOps Cafe Flux

Demonstration of GitOps Toolkit, aka Flux v2 (https://toolkit.fluxcd.io/).

**Note:** The guide start from a freshly created Kubernetes cluster, you can find a simple tutorial on how to create a k0s cluster inside the `k0s-setup/` folder of this repo.

---

## GitOps Toolkit (Flux v2) Presentation

### Bootstrap Flux:
- Create an access token for flux (create it from your git/repo service profile).
- For simplicity I already saved the access token as a secret in the cluster: `kubectl create secret generic gitlab-token --from-literal=token=...`
- Export the gitlab token: `export GITLAB_TOKEN=$(kubectl get secret gitlab-token --template={{.data.token}} | base64 --decode)`.
- Bootstrap flux: `flux bootstrap gitlab \
  --owner=ciori \
  --repository=devops-cafe-flux \
  --branch=master \
  --path=cluster-resources \
  --token-auth \
  --personal`

### Install Rancher with flux:
- Install the necessary components by "enabling" them as code from the repo folder managed by flux.

### Test Rancher access
- Add `192.168.1.139 rancher.devops.cafe` to `/etc/hosts`.
- Change the Rancher service to type NodePort.

### Topics to cover:
- GitOps introduction, two important principles:
    - Alternative Continuous Delivery approach.
    - Git based resources at scale.
- Flux Architecture and Resources.
- Demo.
- Multi cluster management:
    - https://github.com/fluxcd/flux2-kustomize-helm-example/tree/main/apps.
    - https://github.com/fluxcd/flux2-multi-tenancy.
- Considerations on tests, manual interventions, reconcile/suspend and what is deleted.
- GitOps extra: image-controller: https://toolkit.fluxcd.io/guides/image-update/.
