# K0s Cluster Setup Guide

Simple guide to create and manage a k0s cluster (https://github.com/k0sproject/k0s) on a single node using the k0sctl command line (https://github.com/k0sproject/k0sctl):
- Make sure to have a freshly created virtual machine where you want k0s to run.
- Install `k0sctl` on your local pc. Download it from the releases page of the project: https://github.com/k0sproject/k0sctl/releases.
- Make sure your local pc has ssh access to the virtual machine.
- Produce the default cluster config: `k0sctl init --k0s > k0sctl.yaml`.
- Modify the cluster config based on your environment: `vim k0sctl.yaml`
    - role and address of hosts
    - k0s version
    - cluster names
    - api sans and address
    - etcd peer address
    - telemetry
- Create the cluster: `k0sctl apply --config k0sctl.yaml`.
- Get the admin kubeconfig: `k0sctl kubeconfig > kubeconfig`.
- Save/backup `k0sctl.yaml` as it is your cluster configuration and it is used to update it.
- Upgrade the cluster by applying the updated config: `k0sctl apply --config k0sctl.yaml`.
